package com.ppb.adam.pagingapi.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

/**
 * Immutable model class for a Github repo that holds all the information about a repository.
 * Objects of this type are received from the Github API, therefore all the fields are annotated
 * with the serialized name.
 * This class also defines the Room repos table, where the repo [id] is the primary key.
 */
@Entity(tableName = "repos")
data class Repo(
        @PrimaryKey @field:SerializedName("ID") val id: Long,
        @field:SerializedName("Nama") val name: String?,
        @field:SerializedName("Umur") val umur: String?


)
