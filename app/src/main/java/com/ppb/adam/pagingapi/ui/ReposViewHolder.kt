package com.ppb.adam.pagingapi.ui

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.ppb.adam.pagingapi.R
import com.ppb.adam.pagingapi.model.Repo


/**
 * View Holder for a [Repo] RecyclerView list item.
 */
class RepoViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val name: TextView = view.findViewById(R.id.repo_name)
    private val umur: TextView = view.findViewById(R.id.repo_umur)


    private var repo: Repo? = null


    fun bind(repo: Repo?) {
        if (repo == null) {
            val resources = itemView.resources
//            name.text = resources.getString(R.string.loading)
            name.visibility = View.GONE
            umur.visibility = View.GONE
        } else {
            showRepoData(repo)
        }
    }

    private fun showRepoData(repo: Repo) {
        this.repo = repo
        name.text = repo.name

        // if the description is missing, hide the TextView
        var descriptionVisibility = View.GONE
        if (repo.name != null) {
            name.text = repo.name
            descriptionVisibility = View.VISIBLE
        }
        name.visibility = descriptionVisibility

    }

    companion object {
        fun create(parent: ViewGroup): RepoViewHolder {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.repo_view_item, parent, false)
            return RepoViewHolder(view)
        }
    }
}
